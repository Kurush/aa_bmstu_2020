import unittest
from search import *
from itertools import combinations


class SortsTest(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.data = dict()
        for i, k in enumerate(combinations('abcd', 4)):
            self.data[''.join(k)] = i

        self.d = Dictionary(self.data)

    def test_not_found(self):
        self._test('xyz')
        self._test('')
        self._test('xyzabcxyz')

    def test_basic(self):
        for k in self.data.keys():
            self._test(k)

    def _test(self, k):
        v1 = self.d.find_simple(k)
        v2 = self.d.find_binsearch(k)
        v3 = self.d.find_freq(k)
        v = self.data.get(k)

        self.assertEqual(v, v1)
        self.assertEqual(v, v2)
        self.assertEqual(v, v3)


if __name__ == '__main__':
    unittest.main()
