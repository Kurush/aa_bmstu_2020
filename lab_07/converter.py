f = open('elven_raw.txt')

data = dict()

for s in f:
    if len(s.strip()) == 0: continue
    k = s.find('-')
    if k < 0: continue
    rus = s[:k].strip()
    elven = s[k+1:].strip()
    data[elven] = rus

f.close()


with open('elven_dict.txt', 'w') as f:
    for e, r in data.items():
        f.write(e + ' - ' + r + '\n')
