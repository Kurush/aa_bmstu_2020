import sys
sys.path.append("..")
import json

from console.console import *
from termcolor import colored
import matplotlib.pyplot as plt
import numpy as np
import os

import search
from measure_time import *

search_name = {'s1': 'linear',
        's2': 'binsearch (sorted)',
        's3': 'freq-optimized',
        }

DICT = search.Dictionary()

def choose_function(d: search.Dictionary, algo):
    if algo == 's1':
        f = d.find_simple
    elif algo == 's2':
        f = d.find_binsearch
    elif algo == 's3':
        f = d.find_freq
    else:
        f = None
    return f

def search_value(data):
    try:
        f = choose_function(DICT, data.type)
        key = data.key
        value = f(key)
        if value is None:
            value = 'Not found'

        info = 'search type:' + search_name[data.type]

        print(info)
        print('rus: %s, elven: %s' % (key, value))
        if data.analyse:
            tm = measure_time(f, key, _n=1000)
            print('execute time:', round(tm * 1000, 4), 'msecs')
        print('====----====')
    except Exception as e:
        print(colored("error: " + str(e), 'red'))
        return


def analyse(_t=0.1, percent=1, for_graph=False, use_s1=True):
    if use_s1:
        times = {'s1': dict(), 's2': dict(), 's3': dict()}
    else:
        times = {'s2': dict(), 's3': dict()}
    for t in times.values():
        t['min'] = 1e9
        t['max'] = -1e9
        t['avg'] = 0

    DICT.set_n(percent)

    for k in DICT.data.keys():
        for tp in times.keys():
            f = choose_function(DICT, tp)
            t = measure_time(f, k, _n=_t, absolute_time=True)
            times[tp]['min'] = min(times[tp]['min'], t)
            times[tp]['max'] = max(times[tp]['max'], t)
            times[tp]['avg'] += t
    for t in times.values():
        t['avg'] /= len(DICT.data.keys())
        t['min'] = round(t['min']*1e6, 2)
        t['max'] = round(t['max']*1e6, 2)
        t['avg'] = round(t['avg']*1e6, 2)

    for k in 's1 s2 s3'.split():
        if times.get(k) is None: continue
        times[search_name[k]] = times[k]
        times.pop(k)

    if for_graph:
        return times

    print(times)
    with open('analysis.json', 'w') as f:
        json.dump(times, f, indent=2)


def graph(data):
    DICT.reserve(data.right)

    if data.right > 1010:
        use_s1 = False
        times = {'s2': [], 's3': []}
    else:
        use_s1 = True
        times = {'s1': [], 's2': [], 's3': []}

    rg = range(data.left, data.right, data.step)
    n0 = data.right
    for n in rg:
        print('calculating: n=', n)
        t = analyse(data.time, n / n0, True, use_s1=use_s1)
        for k in times.keys():
            times[k].append(t[search_name[k]][data.case])

    rg = list(rg)
    with open('measure_results.txt', 'w') as f:
        for algo in times.keys():
            f.write(algo + ' ')
        f.write('\n')
        for i in range(len(rg)):
            f.write(str(rg[i]) + ' ')
            for algo in times.keys():
                f.write(str(round(times[algo][i], 2)) + ' ')
            f.write('\n')

    legend = []
    for algo in times.keys():
        plt.plot(rg, times[algo])
        legend.append(search_name[algo])
    plt.legend(legend)

    plt.ylabel('время выполнения, мкс.')
    plt.xlabel('размер словаря, n')
    plt.title('Сравнительный анализ алгоритмов: %s' % data.case)

    plt.xticks(range(data.left, data.right, int((data.right - data.left) // 9)))
    plt.show()

def create_console_parser():
    parser = ThrowingArgumentParser(prog='PROG')
    subparsers = parser.add_subparsers()

    x = subparsers.add_parser(name='find', prog='find', help="find key in dictionary")
    x.add_argument('key', type=str, help='key')
    x.add_argument('-t', '--type', type=str, default='s1', help='one of: s1, s2, s3')
    x.add_argument('-a', '--analyse', action='store_const', const=True, help='measure time')
    x.set_defaults(func=lambda data: search_value(data))

    x = subparsers.add_parser(name='analyse', prog='analyse', help="compare algorithms work time")
    x.set_defaults(func=lambda data: analyse())

    x = subparsers.add_parser(name='graph', prog='graph', help="compare algorithms work time")
    x.add_argument('-l', '--left', type=int, default=100, help='left string-length border')
    x.add_argument('-r', '--right', type=int, default=1001, help='right string-length border')
    x.add_argument('-s', '--step', type=int, default=100, help='step')
    x.add_argument('-t', '--time', type=float, default=0.0001, help='time in secs for each measurement')
    x.add_argument('-c', '--case', type=str, default='avg', help='one of: avg(default), min, max')
    x.set_defaults(func=lambda data: graph(data))

    return parser


if __name__ == '__main__':
    parser = create_console_parser()

    hello = '''This software is used to calculate Levenstein and Damerau-Levenstein distances between two strings using
different algorithms. Also it can calculate execution time. To input an empty string, type '' (two single quotes).'''
    CONSOLE = User_console(parser, hello=hello)
    print('>> ', end='')
    while True:
        CONSOLE.read_line()
        print('>> ', end='')