import numpy as np

class SimpleDict:
	def __init__(self, data: dict):
		self.keys, self.values = [], []
		for k, v in data.items():
			self.keys.append(k)
			self.values.append(v)

		self.n = len(self.keys)

	def search(self, key):
		for i in range(self.n):
			if key == self.keys[i]:
				return self.values[i]
		return None


class BinsearchDict:
	def __init__(self, data):
		d = []
		for k, v in data.items():
			d.append([k, v])

		d.sort(key=lambda x: x[0])
		self.keys, self.values = [], []
		for x in d:
			self.keys.append(x[0])
			self.values.append(x[1])

		self.n = len(self.keys)

	def reserve(self, n_new):
		if self.n < n_new:
			for i in range(n_new - self.n):
				self.keys.append('abcd')
				# not adding values
		self.n = len(self.keys)

	def search(self, key):
		l, r = 0, self.n - 1
		while l <= r:
			m = (l + r) // 2
			if key < self.keys[m]:
				r = m - 1
			elif key > self.keys[m]:
				l = m + 1
			else:
				return self.values[m]

		return None


class FreqDict:
	class Trie:
		def __init__(self, useFreq=True):
			self.useFreq = useFreq
			self.root = [dict(), 0, None]
			self.n = 0

		def add(self, x, val, node=None):
			if node is None:
				node = self.root
				self.n += 1
			if not x:
				node[2] = val
				return
			c = x[0]

			if node[0].get(c) is None:
				node[0][c] = [dict(), 0, None]
			node[1] += 1
			self.add(x[1:], val, node[0][c])

		def use_freq_arrays(self, node=None):
			if node is None:
				node = self.root

			keys = list(node[0].keys())
			vals = list(node[0].values())
			for nd in vals:
				self.use_freq_arrays(nd)

			node[0] = keys
			node.append(node[2])
			node[2] = node[1]
			node[1] = vals

			# frequencies sort
			if self.useFreq:
				pairs = [[x,y] for x,y  in zip(node[0], node[1])]
				pairs.sort(key=lambda x: x[1][2], reverse=True)
				node[0] = [p[0] for p in pairs]
				node[1] = [p[1] for p in pairs]


		def find(self, key, node=None):
			if node is None:
				node = self.root
			if not key:
				return node[3]
			c = key[0]

			for i in range(len(node[0])):  # for keys
				if c == node[0][i]:
					return self.find(key[1:], node[1][i])
			return None

	def __init__(self, data):
		self.trie = self.Trie()
		for k,v in data.items():
			self.trie.add(k, v)

		self.trie.use_freq_arrays()

	def search(self, key):
		return self.trie.find(key)


class Dictionary:
	def __init__(self, data=None):
		self.data = data
		if data is None:
			self.data = self._load_dict()

		self.simple = SimpleDict(self.data)
		self.binsearch = BinsearchDict(self.data)
		self.freq = FreqDict(self.data)
		self.n0 = len(self.data)
		self.n = self.n0
		self.n_reserved = self.n0

	def reserve(self, new_n):
		self.binsearch.reserve(new_n)
		self.n_reserved = new_n

	def set_n(self, percent=1):
		self.n = int(self.n0 * percent)
		self.simple.n = self.n
		self.binsearch.n = int(self.n_reserved * percent)
		self.freq.n = self.n

	def find_simple(self, key):
		return self.simple.search(key)

	def find_binsearch(self, key):
		return self.binsearch.search(key)

	def find_freq(self, key):
		return self.freq.search(key)

	def _load_dict(self):
		rus_elven = dict()
		with open('elven_dict.txt') as f:
			for s in f:
				if len(s.strip()) == 0: continue
				k = s.find('-')
				if k < 0: continue
				elven = s[:k].strip()
				rus = s[k + 1:].strip()
				rus_elven[rus] = elven

		return rus_elven