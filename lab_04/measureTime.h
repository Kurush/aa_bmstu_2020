//
// Created by kurush on 15.10.2020.
//

#ifndef LAB4_MEASURETIME_H
#define LAB4_MEASURETIME_H

#include <chrono>
#include <thread>

const int SYSTEM_THREAD_CNT = std::thread::hardware_concurrency();

#define timeStampCount 64
using timeStamp = std::chrono::system_clock::time_point;
static timeStamp startTime, startTimes[timeStampCount], endTimes[timeStampCount];
static double timesAccumulator[timeStampCount] = {0};

#define resetAccumulators for(int i = 0; i < timeStampCount; ++i) timesAccumulator[i] = 0

#define startMeasureTime startTime = (std::chrono::system_clock::now())
#define endMeasureTime ((std::chrono::system_clock::now() - startTime).count() / 1e6)

#define startMeasureTimeStamp(X) startTimes[X] = (std::chrono::system_clock::now())
#define endMeasureTimeValue(X) ((std::chrono::system_clock::now() - startTimes[X]).count() / 1e6)
#define endMeasureTimeIncrement(X) timesAccumulator[(X)] += ((std::chrono::system_clock::now() - startTimes[X]).count() / 1e6)

#define measureTimeStamp(X) ((endTimes[X] - startTimes[X]).count() / 1e6)
#define measureTimeAccumulator(X) (timesAccumulator[X])

#endif //LAB4_MEASURETIME_H
