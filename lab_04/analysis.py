import matplotlib.pyplot as plt
import sys

if __name__ == '__main__':
    times = [[],[],[]]
    with open('results.txt') as f:
        n = int(f.readline())
        times[0].append(float(f.readline()))
        x = f.readline()
        while x != '*\n':
            times[1].append(float(x))
            x = f.readline()
        for x in f.readlines():
            times[2].append(float(x))

    times[0] = [times[0][0]] * len(times[1])
    threads = [2**i for i in range(len(times[0]))]

    print(times)
    for t in times:
        plt.plot(threads, t)
    plt.legend(['no-threads', 'simple', 'advanced'])

    plt.ylabel('время выполнения, мс.')
    plt.xlabel('число потоков, n')
    plt.title('Сортировка слиянием, длина массива: %d' % n)

    plt.xticks(threads)
    plt.show()