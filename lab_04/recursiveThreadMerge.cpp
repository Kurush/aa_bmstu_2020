//
// Created by kurush on 15.10.2020.
//

#include "recursiveThreadMerge.h"

using namespace std;

void recursiveThreadMergeSortInner(vector<int> &a, int l, int r, int threads_available)
{
    if (l >= r) return;
    int m = (l+r)/2;
    if (threads_available == 1) {
        mergeSortInner(a, l, m);
        mergeSortInner(a, m + 1, r);
    }
    else {
        auto t = thread(recursiveThreadMergeSortInner, ref(a), l, m,
                        threads_available/2 + threads_available%2);
        recursiveThreadMergeSortInner(a, m+1, r, threads_available/2);
        if (t.joinable()) t.join();
    }
    merge(a, l, r);
};

void recursiveThreadMergeSort(vector<int> &a, int thread_cnt) {
    recursiveThreadMergeSortInner(a, 0, a.size()-1, thread_cnt);
}