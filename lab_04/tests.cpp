//
// Created by kurush on 16.10.2020.
//


#include <vector>
#include <functional>
#include <cstring>

#include <gtest/gtest.h>

#include "simpleMerge.h"
#include "simpleThreadMerge.h"
#include "recursiveThreadMerge.h"

using namespace std;


bool test(vector<int> &a, const vector<int> &res,
        function<void(vector<int> &, int)> sorter,
        int thread_cnt=1) {
    sorter(a, thread_cnt);
    for (int i = 0; i < a.size(); ++i)
        if (a[i] != res[i]) return false;
    return true;
}

bool testAll(const vector<int> &a0, const vector<int> &res) {
    bool ok = true;
    vector<int> a = a0;
    ok &= test(a, res, [](vector<int> &a, int x) { mergeSort(a); }, 1);
    for (int i = 1; i < 32 && ok; i *= 2) {
        a = a0;
        ok &= test(a, res, simpleThreadMergeSort, i);
        a = a0;
        ok &= test(a, res, recursiveThreadMergeSort, i);
    }
    return ok;
}

TEST(SortTests, empty) {EXPECT_EQ(testAll({}, {}), true);}
TEST(SortTests, single) {EXPECT_EQ(testAll({2}, {2}), true);}
TEST(SortTests, sorted) {EXPECT_EQ(testAll({1, 2, 3, 4, 5, 6, 7, 8,  9, 10},
        {1, 2, 3, 4, 5, 6, 7, 8,  9, 10}), true);}
TEST(SortTests, reverse_sorted) {EXPECT_EQ(testAll({5, 4, 3, 2, 1, 0, -1, -2, -3, -4},
        {-4, -3, -2, -1, 0, 1, 2, 3, 4, 5}), true);}
TEST(SortTests, mixed) {EXPECT_EQ(testAll({1, -1, 2, -2, 3, -3, 4, -4, 0, 0},
                                                   {-4, -3, -2, -1, 0, 0, 1, 2, 3, 4}), true);}
TEST(SortTests, triple) {EXPECT_EQ(testAll({3, 1, 2}, {1, 2, 3}), true);}
TEST(SortTests, quad) {EXPECT_EQ(testAll({4, 1, 2, 3}, {1, 2, 3, 4}), true);}

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    int err = RUN_ALL_TESTS();
    return err;
}