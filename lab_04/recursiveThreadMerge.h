//
// Created by kurush on 15.10.2020.
//

#ifndef LAB4_RECURSIVETHREADMERGE_H
#define LAB4_RECURSIVETHREADMERGE_H

#include <vector>
#include <thread>
#include <mutex>
#include <iostream>
#include <functional>

#include "simpleMerge.h"
#include "measureTime.h"

void recursiveThreadMergeSortInner(std::vector<int> &a, int l, int r,
        int thread_available);

void recursiveThreadMergeSort(std::vector<int> &a, int thread_cnt=SYSTEM_THREAD_CNT);

#endif //LAB4_RECURSIVETHREADMERGE_H
