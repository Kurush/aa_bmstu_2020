//
// Created by kurush on 15.10.2020.
//

#ifndef LAB4_SIMPLEMERGE_H
#define LAB4_SIMPLEMERGE_H

#include <vector>

void merge(std::vector<int> &a, int l, int r);

void mergeSortInner(std::vector<int> &a, int l, int r);
void mergeSort(std::vector<int> &a);


#endif //LAB4_SIMPLEMERGE_H
