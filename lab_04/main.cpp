#include <iostream>
#include <vector>
#include <iostream>
#include <fstream>
#include <functional>
#include <cstring>

#include "simpleMerge.h"
#include "simpleThreadMerge.h"
#include "recursiveThreadMerge.h"
#include "measureTime.h"

using namespace std;

void print(vector<int> &a) {
    for (auto &x: a)
        cout << x << ' ';
    cout << '\n';
}

void measureThreadTimes(int arr_size = 10000, int check_times = 100, int coef = 8) {
    vector<int> thread_cnts;
    for (int i = 1; i <= coef * SYSTEM_THREAD_CNT; i *= 2)
        thread_cnts.push_back(i);

    vector<int> arr0(arr_size), a;
    for (int i = 0; i < arr_size; ++i)
        arr0[i] = arr_size-i;

    int offset;
    for (int i = 0; i < check_times; ++i) {
        a = arr0;
        startMeasureTimeStamp(0);
        mergeSort(a);
        endMeasureTimeIncrement(0);

        offset = 1;
        for (int t = 0; t < thread_cnts.size(); ++t) {
            a = arr0;
            startMeasureTimeStamp(offset+t);
            simpleThreadMergeSort(a, thread_cnts[t]);
            endMeasureTimeIncrement(offset+t);
        }

        offset = 1 + thread_cnts.size();
        for (int t = 0; t < thread_cnts.size(); ++t) {
            a = arr0;
            startMeasureTimeStamp(offset+t);
            recursiveThreadMergeSort(a, thread_cnts[t]);
            endMeasureTimeIncrement(offset+t);
        }

    }

    ofstream file;
    file.open("results.txt");

    cout << "time measurements for merge sort of array with size = " << arr_size << ":\n";
    file << arr_size << '\n';
    cout << "no-threads: " << measureTimeAccumulator(0) / check_times << "ms\n";
    file << measureTimeAccumulator(0) / check_times << '\n';
    for (int i = 0; i < thread_cnts.size(); ++i) {
        cout << "simple threading, n=" << thread_cnts[i] << ": "
             << measureTimeAccumulator(i + 1) / check_times << "ms\n";
        file << measureTimeAccumulator(i+1) / check_times << '\n';
    }
    file << "*\n";
    for (int i = 0; i < thread_cnts.size(); ++i) {
        cout << "recursive threading, n=" << thread_cnts[i] << ": "
             << measureTimeAccumulator(i + offset) / check_times << "ms\n";
        file << measureTimeAccumulator(i+offset) / check_times << '\n';
    }
    file.close();
}

int main(int argc, char* argv[]) {
    if (argc == 1) {
        cout << "arguments are needed!";
        return 1;
    }
    if (strcmp(argv[1],"time") == 0) {
        int n = 10000;
        int coef=4;
        if (argc > 2)
            n = atoi(argv[2]);
        if (argc > 3)
            coef = atoi(argv[3]);
        measureThreadTimes(n, 100, coef);
        system("python3.6 analysis.py");
        return 0;
    }

    int n;
    vector<int> a;

    ifstream file;
    file.open (argv[1]);
    if (!file.is_open()) {
        cout << "file not found!";
        return 1;
    }
    file >> n;
    a.resize(n);
    for (int i = 0; i < n; ++i)
        file >> a[i];
    file.close();

    cout << "sorting array: ";
    print(a);

    function<void(vector<int>&, int)> f;
    if (argc > 2) {
        if (strcmp(argv[2],"simple") == 0) f = simpleThreadMergeSort, cout << "merge sort, simple threading\n";
        else if (strcmp(argv[2], "recursive") == 0) f = simpleThreadMergeSort, cout << "merge sort, recursive threading\n";
        else f = [](vector<int>&a, int x) {mergeSort(a);}, cout << "merge sort, no threading\n";
    }
    else f = simpleThreadMergeSort, cout << "merge sort, no threading\n";

    f(a, SYSTEM_THREAD_CNT);
    cout << "result: ";
    print(a);


    return 0;
}
