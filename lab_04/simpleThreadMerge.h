//
// Created by kurush on 15.10.2020.
//

#ifndef LAB4_SIMPLETHREADMERGE_H
#define LAB4_SIMPLETHREADMERGE_H

#include <vector>
#include <thread>
#include <mutex>
#include <iostream>
#include <functional>

#include "simpleMerge.h"
#include "measureTime.h"

void accumulate(std::vector<int> &accumulator, std::vector<int> &a, int l, int r);

void simpleThreadMergeSortInner(std::vector<int> &a, int l, int r,
        std::mutex &mtx, std::function<void(int, int)> f);

void simpleThreadMergeSort(std::vector<int> &a, int thread_cnt=SYSTEM_THREAD_CNT);

#endif //LAB4_SIMPLETHREADMERGE_H
