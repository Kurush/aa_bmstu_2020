//
// Created by kurush on 15.10.2020.
//

#include "simpleThreadMerge.h"

using namespace std;

void accumulate(vector<int> &accumulator, vector<int> &a, int l, int r) {
    static vector<int> tmp;
    int n = (r-l+1) + accumulator.size();
    tmp.resize(n);
    int p1 = 0, p2 = l, n1=accumulator.size(), n2=r;

    for(int i = 0; i < n; ++i)
        if ((p1 < n1) && ((p2 > n2) || (accumulator[p1] < a[p2])))
            tmp[i] = accumulator[p1], p1++;
        else
            tmp[i] = a[p2], p2++;

    accumulator.resize(n);
    for (int i = 0; i < n; ++i)
        accumulator[i] = tmp[i];
};

void simpleThreadMergeSortInner(vector<int> &a, int l, int r,
        mutex &mtx, function<void(int, int)> accumulate)
{
    mergeSortInner(a, l, r);
    mtx.lock();
    accumulate(l, r);
    mtx.unlock();
};

void simpleThreadMergeSort(vector<int> &a, int thread_cnt) {
    if (a.empty()) return;
    thread threads[thread_cnt];
    mutex mtx;
    int n = a.size();
    int thread_size = n / thread_cnt, remain = n % thread_cnt;
    int l = 0, r = thread_size-1 + (remain > 0);

    vector<int> accumulator(0);
    auto f = [&accumulator, &a](int l, int r) {
        accumulate(accumulator, a, l, r);
    };

    for (int i = 0; i < thread_cnt; ++i) {
        threads[i] = thread(simpleThreadMergeSortInner, ref(a), l, r, ref(mtx), f);
        l = r+1;
        r = l + thread_size-1 + (i+1 < remain);
    }
    for (size_t i = 0; i < thread_cnt; ++i)
        if (threads[i].joinable()) threads[i].join();

    for (int i = 0; i < a.size(); ++i)
        a[i] = accumulator[i];
}