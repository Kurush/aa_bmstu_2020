//
// Created by kurush on 15.10.2020.
//

#include "simpleMerge.h"

using namespace std;

void merge(vector<int> &a, int l, int r)
{
    static vector<int> tmp;
    tmp.resize(a.size());
    int m = (l+r)/2, p1 = l, p2 = m+1;

    for(int i = l; i <= r; ++i)
        if ((p1 <= m) && ((p2 > r) || (a[p1] < a[p2])))
            tmp[i] = a[p1], p1++;
        else
            tmp[i] = a[p2], p2++;

    for (int i = l; i <= r; ++i)
        a[i] = tmp[i];
};

void mergeSortInner(vector<int> &a, int l, int r)
{
    if (l >= r) return;
    int m = (l+r)/2;
    mergeSortInner(a, l, m);
    mergeSortInner(a, m+1, r);
    merge(a, l, r);
};

void mergeSort(std::vector<int> &a) {
    mergeSortInner(a, 0, a.size()-1);
}