import console.my_argparse as argparse
import colorama
from colorama import Fore, Back, Style

colorama.init()

class ArgumentParserError(Exception): pass

class ThrowingArgumentParser(argparse.ArgumentParser):
    def error(self, message):
        raise ArgumentParserError(message)

parser = ThrowingArgumentParser()

class User_console():
    def __init__(self, parser, hello=None):
        # commands: dictionary string : function (string is a comand name)
        self.parser = parser
        self.hello = hello
        self.print_hello()


    def print_hello(self):
        if self.hello: print(self.hello)
        print("Type '-h' or '--help' to get more info")
        print("Enter your commands:")

    def read_line(self):
        try:
            s = input().split()
            args = self.parser.parse_args(s)
            if args.__contains__('func'):
                args.func(args)
        except Exception as ex:
            print(Fore.RED + str(ex))
            print(Style.RESET_ALL, end='')
