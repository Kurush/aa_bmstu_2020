def restore_table_ans(table, restorer):
    n, m = len(table), len(table[0])
    i, j, res = n-1, m-1, ''
    while i or j:
        i, j, c = restorer(i, j)
        res = c + res
    return res

def levenshtein(table, s1, s2):
    def lev(i, j, vals=None, get_move=False, keep_3_format=False):
        if i == 0 and j == 0: res = 0
        elif i == 0: res = j
        elif j == 0: res = i
        else:
            if vals is None:
                vals = [table[i][j-1], table[i-1][j], table[i-1][j-1]]
            res = min(vals[0] + 1, vals[1] + 1,
                      vals[2] + (0 if s1[i-1] == s2[j-1] else 1))

        if get_move:
            if i == 0 and j == 0: move, choice = '', 0
            elif i == 0: move, choice = 'I', 0
            elif j == 0: move, choice = 'D', 1
            else:
                if s1[i-1] == s2[j-1] and vals[2] == res:
                    move = 'M'
                else:
                    move = {vals[0] + 1: 'I', vals[1] + 1: 'D',
                            vals[2] + 1: 'R'}[res]
                choice = 0 if move=='I' else (1 if move=='D' else 2)
            return res, move, choice
        elif keep_3_format:
            return res, None, None

        return res

    def restorer(i, j):
        if i == 0 and j == 0: p, q, c = 0, 0, ''
        elif i == 0: p, q, c = 0, j-1, 'I'
        elif j == 0: p, q, c = i-1, 0, 'D'
        else:
            if table[i-1][j-1] == table[i][j] and s1[i-1] == s2[j-1]: p, q, c = i-1, j-1, 'M'
            elif table[i-1][j-1] == table[i][j] - 1: p, q, c = i-1, j-1, 'R'
            elif table[i-1][j] == table[i][j] - 1: p, q, c = i-1, j, 'D'
            elif table[i][j-1] == table[i][j] - 1: p, q, c = i, j-1, 'I'
            else: p, q, c = None, None, None
        return p, q, c

    return lev, restorer


def damerau_levenshtein(table, s1, s2):
    lev, lev_restorer = levenshtein(table, s1, s2)

    def dl(i, j, vals=None, get_move=False):
        res, move, choice = lev(i, j, vals, get_move=get_move, keep_3_format=True)
        if i > 1 and j > 1:
            if vals is None:
                vals = {3: table[i - 2][j - 2]}
            if s1[i-2] == s2[j-1] and s1[i-1] == s2[j-2] and res > vals[3] + 1:
                res, move, choice = vals[3] + 1, 'T', 3

        return (res, move, choice) if get_move else res

    def dl_restorer(i, j):
        p, q, c = lev_restorer(i,j)
        if i > 1 and j > 1:
            if table[i-2][j-2] == table[i][j]-1 and s1[i-2]==s2[j-1] and s1[i-1]==s2[j-2]:
                p, q, c = i-2, j-2, 'T'
        return p, q, c

    return dl, dl_restorer
