import numpy as np
from itertools import product

import lab_01.levenstein_metric as metrics


def levenstein_tabled(s1, s2, metric_creator=metrics.levenshtein):
    n, m = len(s1)+1, len(s2)+1
    table = np.zeros((n, m), dtype=np.uint16)
    metric, restorer = metric_creator(table, s1, s2)

    for i, j in product(range(n), range(m)):
        table[i][j] = metric(i, j)

    res = metrics.restore_table_ans(table, restorer)
    return res, table


def levenstein_recursive_table(s1, s2):
    n, m = len(s1)+1, len(s2)+1
    inf = n+m
    table = np.full([n, m], inf, dtype=np.uint16)
    metric, restorer = metrics.levenshtein(table, s1, s2)

    def recurse(i, j):
        if i < 0 or j < 0: return
        if table[i][j] == inf:
            recurse(i-1, j), recurse(i, j-1), recurse(i-1, j-1)
            table[i][j] = metric(i, j)

    recurse(n-1, m-1)

    res = metrics.restore_table_ans(table, restorer)
    return res, table


def levenstein_recursive(s1, s2):
    n, m = len(s1)+1, len(s2)+1
    metric, _ = metrics.levenshtein([], s1, s2)

    def recurse(i, j):
        if i < 0 or j < 0: return np.Inf, ''
        choices = [recurse(i, j-1), recurse(i-1, j), recurse(i-1, j-1)]
        values = [choices[0][0], choices[1][0], choices[2][0]]
        res, move, choice = metric(i, j, vals=values, get_move=True)
        return res, choices[choice][1] + move

    res, path = recurse(n-1, m-1)
    return path
