import sys
sys.path.append("..")

from console.console import *
from termcolor import colored
from time import process_time, time
import matplotlib.pyplot as plt

import lab_01.string_distances as dist
import lab_01.levenstein_metric as metrics
from measure_time import *

def choose_function(algo):
    if algo == 'l1':
        f = dist.levenstein_tabled
    elif algo == 'l2':
        f = lambda *args: (dist.levenstein_recursive(*args), None)
    elif algo == 'l3':
        f = dist.levenstein_recursive_table
    elif algo == 'dl':
        f = lambda *args: dist.levenstein_tabled(*args, metric_creator=metrics.damerau_levenshtein)
    else:
        f = None
    return f

def lev_dist(data, algo=None):
    try:
        assert(algo is not None)

        if data.s1 == "''": data.s1 = ''
        if data.s2 == "''": data.s2 = ''

        f = choose_function(algo)
        res, table = f(data.s1, data.s2)

        time = measure_time(f, data.s1, data.s2, _n=1000) if data.analyse else None
        info = {'l1': 'metric: levenshtein distance, algorithm: using matrix',
                'l2': 'metric: levenshtein distance, algorithm: recursive, without matrix',
                'l3': 'metric: levenshtein distance, algorithm: recursive, using matrix',
                'dl': 'metric: damerau-levenshtein distance, algorithm: using matrix'}[algo]

        if res == '': res = "''"
        if data.s1 == '': data.s1 = "''"
        if data.s2 == '': data.s2 = "''"
        output(res, table if data.table else None, data.s1, data.s2, info,
               time=time)

    except Exception as e:
        print(colored("error: " + str(e), 'red'))
        return


def graph(data):
    times = {'l1': [], 'l3': [], 'dl': []}
    if data.recursive:
        times['l2'] = []
    rg = range(data.left, data.right, data.step)
    for n in rg:
        s1 = s2 = 'a' * n
        for algo in times.keys():
            f = choose_function(algo)
            times[algo].append(measure_time(f, s1, s2, absolute_time=True, _n=data.time)*1000)

    rg = list(rg)
    with open('measure_results.txt', 'w') as f:
        for algo in times.keys():
            f.write(algo + ' ')
        f.write('\n')
        for i in rg:
            f.write(str(i) + ' ')
            for algo in times.keys():
                f.write(str(round(times[algo][i], 2)) + ' ')
            f.write('\n')

    for algo in times.keys():
        plt.plot(rg, times[algo])
    plt.legend(times.keys())
    if data.russian == True:
        plt.ylabel('время выполнения, мс.')
        plt.xlabel('длина строки, n')
    else:
        plt.ylabel('execute time, msec')
        plt.xlabel('string length, n')
    plt.xticks(range(data.left, data.right, (data.right-data.left) // 5))
    plt.show()


def output(result, table, s1, s2, data=None, time=None):
    print('transforming:', s1, '->', s2)
    if data is not None: print(data)
    if time is not None: print('execute time:', round(time * 1000, 4), 'msecs')
    result_size = len([x for x in result if x != 'M'])
    print('transform sequence:', result)
    print('transform cost:', result_size)
    if table is not None:
        print('transform table:')
        s1 = 'ø' + s1
        s2 = 'ø' + s2
        print('* ', ' '.join(s2), sep='')
        for i, c in enumerate(s1):
            print(c, ' ', sep='', end='')
            for t in table[i]:
                print("%-2i" % t, end='')
            print()
    print('====----====')


def create_console_parser():
    parser = ThrowingArgumentParser(prog='PROG')
    subparsers = parser.add_subparsers()

    x = subparsers.add_parser(name='l1', prog='l1', help="Levenshtein distance, matrix algo")
    x.add_argument('s1', type=str, help='first string')
    x.add_argument('s2', type=str, help='second string')
    x.add_argument('-t', '--table', action='store_const', const=True, help='show table')
    x.add_argument('-a', '--analyse', action='store_const', const=True, help='measure time')
    x.set_defaults(func=lambda data: lev_dist(data, algo='l1'))

    x = subparsers.add_parser(name='l2', prog='l1', help="Levenshtein distance, recursive algo")
    x.add_argument('s1', type=str, help='first string')
    x.add_argument('s2', type=str, default=False, help='second string')
    x.add_argument('-t', '--table', action='store_const', const=False,  help='show table, no use')
    x.add_argument('-a', '--analyse', action='store_const', const=True, help='measure time')
    x.set_defaults(func=lambda data: lev_dist(data, algo='l2'))

    x = subparsers.add_parser(name='l3', prog='l3', help="Levenshtein distance, recursive-matrix algo")
    x.add_argument('s1', type=str, help='first string')
    x.add_argument('s2', type=str, help='second string')
    x.add_argument('-t', '--table', action='store_const', const=True, help='show table')
    x.add_argument('-a', '--analyse', action='store_const', const=True, help='measure time')
    x.set_defaults(func=lambda data: lev_dist(data, algo='l3'))

    x = subparsers.add_parser(name='dl', prog='dl', help="Damerau-Levenshtein distance, matrix algo")
    x.add_argument('s1', type=str, help='first string')
    x.add_argument('s2', type=str, help='second string')
    x.add_argument('-t', '--table', action='store_const', const=True, help='show table')
    x.add_argument('-a', '--analyse', action='store_const', const=True, help='measure time')
    x.set_defaults(func=lambda data: lev_dist(data, algo='dl'))

    x = subparsers.add_parser(name='graph', prog='graph', help="compare algorithms work time")
    x.add_argument('-l', '--left', type=int, default=0, help='left string-length border')
    x.add_argument('-r', '--right', type=int, default=8, help='right string-length border')
    x.add_argument('-s', '--step', type=int, default=1, help='step')
    x.add_argument('-ru', '--russian', action='store_const', const=True, help='use Russian language for graph labels')
    x.add_argument('-rec', '--recursive', action='store_const', const=True, help='compute recursive algorithm')
    x.add_argument('-t', '--time', type=float, default=0.1, help='time in secs for each measurement')
    x.set_defaults(func=lambda data: graph(data))

    return parser


if __name__ == '__main__':
    parser = create_console_parser()

    hello = '''This software is used to calculate Levenstein and Damerau-Levenstein distances between two strings using
different algorithms. Also it can calculate execution time. To input an empty string, type '' (two single quotes).'''
    CONSOLE = User_console(parser, hello=hello)
    print('>> ', end='')
    while True:
        CONSOLE.read_line()
        print('>> ', end='')