import unittest
from .string_distances import *
from .levenstein_metric import *


class LevensteinMetricsTest(unittest.TestCase):
    def test_replace(self):
        self._test('aaa', 'bbb', 'rrr')

    def test_transposition(self):
        self._test('ab', 'ba', 'rr', 't')
        self._test('abcd', 'badc', 'imrmd', 'tt')
        self._test('abc12xyz', 'abc21xyz', 'mmmrrmmm', 'mmmtmmm')

    def test_both_directs(self):
        self._test('aab', 'baa', 'rmr')
        self._test('baa', 'aab', 'rmr')

    def test_empty(self):
        self._test('', '', '')
        self._test('', 'xyz', 'iii')
        self._test('xyzabc', '', 'dddddd')

    def test_almost(self):
        self._test('x12345', '12345', 'dmmmmm')
        self._test('12345', 'x12345', 'immmmm')
        self._test('12345', '12345x', 'mmmmmi')
        self._test('12345x', '12345', 'mmmmmd')

    def test_common(self):
        self._test('abc123xy', '123abcyx', 'iiimmmddrmd', 'rrrrrrt')

    def _test(self, s1, s2, lres, dlres=None):
        dlres = dlres if dlres else lres
        self.assertEqual(levenstein_tabled(s1, s2)[0].lower(), lres)
        self.assertEqual(levenstein_recursive(s1, s2).lower(), lres)
        self.assertEqual(levenstein_recursive_table(s1, s2)[0].lower(), lres)
        self.assertEqual(levenstein_tabled(s1, s2, metric_creator=damerau_levenshtein)[0].lower(), dlres)

if __name__ == '__main__':
    unittest.main()
