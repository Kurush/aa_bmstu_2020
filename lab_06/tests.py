import unittest
from salesman import *
from graph_generator import *

class SortsTest(unittest.TestCase):
    def test_1(self):
        self._test(1)

    def test_5(self):
        for i in range(10):
            self._test(5)

    def test_8(self):
        for i in range(2):
            self._test(8)

    def _test(self, n):
        g = GraphGenerator(1, n)
        a = AntColony()
        b = BruteForce()

        res_a = trainOne(a, g.graphs[0])
        res_b = trainOne(b, g.graphs[0])
        #mx = sum([g.graphs[0][i][(i+1)%n] for i in range(n)])
        self.assertIn(res_a, range(0, 1000, 1))
        self.assertIn(res_b, range(0, 1000, 1))
        self.assertGreaterEqual(res_a, res_b)


if __name__ == '__main__':
    unittest.main()
