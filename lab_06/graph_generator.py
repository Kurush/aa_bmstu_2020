import random
import numpy as np


def random_matrix(n=8):
    x = np.random.normal(8, 3, (n, n)).astype('int')
    for j in range(n):
        x[j][j] = 0
        for i in range(j+1, n):
            x[j][i] = max(1, x[j][i])
            x[i][j] = x[j][i]
    return x


class GraphGenerator:
    def __init__(self, n: int, sz:int=8):
        self.n = n
        self.graphs = [0]*n
        self.size = sz

        self._build()

    def _build(self):
        for i in range(self.n):
            self.graphs[i] = random_matrix(self.size)
