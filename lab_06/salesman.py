import numpy as np
import itertools


# circled paths: 1-2-3-1
class SalesMan:
    def __init__(self, graph=None):
        if graph is not None:
            self.set_graph(graph)

    def set_graph(self, graph):
        self.graph = graph
        self.n = graph.shape[0]
        self.solution = None
        self.best_path = None

    def path_len(self, path):
        s = 0
        for i in range(self.n):
            s += self.graph[path[i]][path[(i + 1) % self.n]]
        return s

    def solve(self):
        pass


class BruteForce(SalesMan):
    def __init__(self, graph=None):
        super(BruteForce, self).__init__(graph)

    def solve(self):
        m = 1e9
        for path in self.build_paths():
            m0 = m
            m = min(m, self.path_len(path))
            if m != m0: self.best_path = path
        self.solution = m
        return m

    def build_paths(self):
        path = list(range(self.n))
        for p in itertools.permutations(path):
            yield p


class AntColony(SalesMan):
    def __init__(self, graph=None):
        super(AntColony, self).__init__(graph)
        self.history = []

    def set_config(self, ant_cnt=None, elite_coef=0.2, start_f=1, min_f_coef=0.1, alpha=1, beta=1, evap=0.2, iter=5):
        if ant_cnt is None:
            ant_cnt = self.n
        if ant_cnt > self.n:
            raise Exception("too many ants")
        self.ant_cnt = ant_cnt
        self.elite = ant_cnt * elite_coef

        self.start_f = start_f
        self.min_f = start_f * min_f_coef

        self.alpha, self.beta = alpha, beta
        self.evap = evap
        self.iter = iter

    def set_graph(self, graph):
        super().set_graph(graph)
        self.Q = np.sum(self.graph) - np.sum(np.diagonal(self.graph))
        self.Q /= self.n
        self.set_config()

    def solve(self, iter=None):
        if iter is not None:
            self.iter = iter

        if self.n < 2:
            return 0

        self.best_path = list(range(self.n))
        self.solution = self.path_len(self.best_path)
        self.history = []
        self.F = np.full((self.n, self.n), self.start_f)

        for _ in range(self.iter):
            self.calc_prob()

            dF = np.full((self.n, self.n), 0)
            for a in range(self.ant_cnt):
                # buid path, update best
                p = self.build_path(a)
                ln = self.path_len(p)
                if ln < self.solution:
                    self.solution, self.best_path = ln, p
                # update F change
                for i in range(self.n):
                    dF[p[i]][p[(i + 1) % self.n]] += self.Q / ln

            # update F
            for i in range(self.n):
                for j in range(self.n):
                    self.F[i][j] = max(self.min_f, (1 - self.evap) * self.F[i][j] + dF[i][j])
            # elite troops
            p, ln = self.best_path, self.solution
            for i in range(self.n):
                self.F[p[i]][p[(i + 1) % self.n]] += self.elite * self.Q / ln

            self.history.append(self.solution)
        return self.solution

    def calc_prob(self):
        self.P = np.full((self.n, self.n), 0, 'float64')
        for i in range(self.n):
            for j in range(self.n):
                if i == j: continue
                self.P[i][j] = float(self.F[i][j] ** self.alpha / self.graph[i][j] ** self.beta)

    def build_path(self, start):
        path = [start]
        unused = set(range(self.n))
        unused.remove(start)
        cur = start
        for i in range(1, self.n):
            s = sum([self.P[cur][i] for i in unused])
            val = np.random.random() * s
            acc = 0
            for new in unused:
                acc += self.P[cur][new]
                if acc + 1e-5 > val:
                    break
            path.append(new)
            unused.remove(new)
            cur = new
        return path


def train(model: SalesMan, graphs):
    ans = []
    for g in graphs:
        model.set_graph(g)
        ans.append(model.solve())
    return ans

def trainOne(model: SalesMan, graph):
    model.set_graph(graph)
    return model.solve()