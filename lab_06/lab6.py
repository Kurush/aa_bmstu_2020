import sys

sys.path.append("..")

from console.console import *
from termcolor import colored
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import itertools

import salesman as sales
import graph_generator as gen
from measure_time import *
import numpy as np

def table(params):
    try:
        data = {
            'elite_coef': [0.1, 0.5, 0.9],
            'start_f': [1],
            'min_f_coef': [0.1],
            'alpha': [0,1],
            'beta': [0,1,2,3,4,5],
            'evap': [0.1, 0.5, 0.9],
        }
        max_iter = 6

        table = []

        m = sales.BruteForce()
        a = sales.AntColony()
        g = gen.GraphGenerator(params.tables, params.n)
        brutes = sales.train(m, g.graphs)

        repeat_cnt = 3

        for x in itertools.product(*data.values()):
            ans = []
            for tb in range(params.tables):
                a.set_graph(g.graphs[tb])
                a.set_config(None, *x, iter=max_iter)
                hist = [1e9] * max_iter
                for _ in range(repeat_cnt):
                    a.solve()
                    hist = [min(hist[i], a.history[i]) for i in range(max_iter)]
                ans.append(hist)
            for i in range(max_iter):
                res = []
                for tb in range(params.tables):
                    res += [ans[tb][i], ans[tb][i] - brutes[tb]]
                table.append(list(x) + [i+1] + res)

        dif = ['diff-%d'%i for i in range(params.tables)]
        path = ['path-%d'%i for i in range(params.tables)]
        add = []
        for d, p in zip(dif, path):
            add += [p, d]
        df = pd.DataFrame(table, columns=list(data.keys())+['iter']+add)
        df = df.drop(['start_f', 'min_f_coef'], axis=1)
        df = df.sort_values(by=['iter', 'elite_coef', 'alpha', 'beta', 'evap'])
        df.to_csv('data.csv', index=False)
        print(brutes)
        print(df)
        print('correlation:')
        print(df.drop(path, axis=1).corr(method='spearman')[dif].drop(dif))


    except Exception as e:
        print(colored("error: " + str(e), 'red'))
        return


def graph(data):
    times = {'brute': [], 'ants': []}
    rg = range(data.left, data.right, data.step)

    m = sales.BruteForce()
    a = sales.AntColony()

    for n in rg:
        g = gen.GraphGenerator(1, n)
        times['brute'].append(measure_time(sales.trainOne, m, g.graphs[0], absolute_time=True, _n=data.time) * 1000)
        times['ants'].append(measure_time(sales.trainOne, a, g.graphs[0], absolute_time=True, _n=data.time) * 1000)

    rg = list(rg)
    with open('measure_results.txt', 'w') as f:
        for algo in times.keys():
            f.write(algo + ' ')
        f.write('\n')
        for i in range(len(rg)):
            f.write(str(rg[i]) + ' ')
            for algo in times.keys():
                f.write(str(round(times[algo][i], 2)) + ' ')
            f.write('\n')

    for algo in times.keys():
        plt.plot(rg, times[algo])
    plt.legend(times.keys())
    plt.ylabel('время выполнения, мс.')
    plt.xlabel('число вершин графа, n')
    plt.title('Сравнительный анализ алгоритмов')
    plt.show()


def create_console_parser():
    parser = ThrowingArgumentParser(prog='PROG')
    subparsers = parser.add_subparsers()

    x = subparsers.add_parser(name='table', prog='table', help="compare algorithms precision")
    x.add_argument('-n', type=int, default=8, help='graph size')
    x.add_argument('-t', '--tables', type=int, default=3, help='tables count')
    x.set_defaults(func=lambda data: table(data))

    x = subparsers.add_parser(name='graph', prog='graph', help="compare algorithms work time")
    x.add_argument('-l', '--left', type=int, default=1, help='left string-length border')
    x.add_argument('-r', '--right', type=int, default=8, help='right string-length border')
    x.add_argument('-s', '--step', type=int, default=1, help='step')
    x.add_argument('-ru', '--russian', action='store_const', const=True, help='use Russian language for graph labels')
    x.add_argument('-t', '--time', type=float, default=0.1, help='time in secs for each measurement')
    x.add_argument('-c', '--case', type=str, default='random', help='one of: best, worst, random(default)')
    x.set_defaults(func=lambda data: graph(data))

    return parser


if __name__ == '__main__':
    parser = create_console_parser()

    hello = '''lab 6'''
    CONSOLE = User_console(parser, hello=hello)
    print('>> ', end='')
    while True:
        CONSOLE.read_line()
        print('>> ', end='')
