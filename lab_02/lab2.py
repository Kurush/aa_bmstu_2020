import sys
sys.path.append("..")

from console.console import *
from termcolor import colored
import matplotlib.pyplot as plt
import numpy as np
import os

import lab_02.matrix_prod as mp
from measure_time import *

algo_name = {'st': 'standart',
        'w1': 'Winograd',
        'w2': 'Winograd, optimized',
        }

def choose_function(algo):
    if algo == 'st':
        f = mp.standart
    elif algo == 'w1':
        f = mp.winograd
    elif algo == 'w2':
        f = mp.winograd_optimized
    else:
        f = None
    return f

def load(f):
    data = []
    with open(f) as f:
        for s in f:
            data.append(list(map(float, s.split())))
    #return np.array(data)
    return data

def prod(data):
    try:
        f = choose_function(data.type)
        m1 = load(os.getcwd() + '/' + data.file1)
        m2 = load(os.getcwd() + '/' + data.file2)
        print('matrix1:\n', m1)
        print('\nmatrix2:\n', m2)
        res = f(m1, m2)
        np.savetxt(os.getcwd() + '/' + 'product.txt', res, newline=' ', fmt='%.2f')
        info = 'product algorithm: ' + algo_name[data.type]

        print(info)
        print('result:', res)
        if data.analyse:
            tm = measure_time(f, m1, m2, _n=1000)
            print('execute time:', round(tm * 1000, 4), 'msecs')
        print('====----====')
    except Exception as e:
        print(colored("error: " + str(e), 'red'))
        return


def graph(data):
    times = {'st': [], 'w1': [], 'w2': []}

    if data.step %2: data.step += 1
    flag = data.case == 'best'
    if data.left %2 == flag: data.left += 1
    if data.right %2 == flag: data.right += 1

    rg = range(data.left, data.right, data.step)
    for n in rg:
        print('calculating: n=', n)
        #m1, m2 = np.random.random((n, n)), np.random.random((n, n))
        m1, m2 = [1]*n, [1]*n
        for i in range(n):
            m1[i], m2[i] = [1]*n, [1]*n
        for algo in times.keys():
            f = choose_function(algo)
            if data.time < 0:
                times[algo].append(measure_time(f, m1, m2, _n=int(-data.time))*1000)
            else:
                times[algo].append(measure_time(f, m1, m2, absolute_time=True, _n=data.time)*1000)

    rg = list(rg)
    with open('measure_results.txt', 'w') as f:
        for algo in times.keys():
            f.write(algo + ' ')
        f.write('\n')
        for i in range(len(rg)):
            f.write(str(rg[i]) + ' ')
            for algo in times.keys():
                f.write(str(round(times[algo][i], 2)) + ' ')
            f.write('\n')

    legend = []
    for algo in times.keys():
        plt.plot(rg, times[algo])
        legend.append(algo_name[algo])
    plt.legend(legend)
    if data.russian == True:
        plt.ylabel('время выполнения, мс.')
        plt.xlabel('линейный размер матрицы, n')
        case = 'лучший' if data.case=='best' else 'худший'
        plt.title('Сравнительный анализ алгоритмов произведения матриц: %s случай' % case)
    else:
        plt.ylabel('execute time, msec')
        plt.xlabel('matrix length, n')
        plt.title('Comparative analysis: %s case' % data.case)
    plt.xticks(range(data.left, data.right, int((data.right - data.left) // 9)))
    plt.show()



def create_console_parser():
    parser = ThrowingArgumentParser(prog='PROG')
    subparsers = parser.add_subparsers()

    x = subparsers.add_parser(name='prod', prog='prod', help="matrix product")
    x.add_argument('-f1', '--file1', type=str, default='m1.txt', help='file with 1st matrix')
    x.add_argument('-f2', '--file2', type=str, default='m2.txt', help='file with 2nd matrix')
    x.add_argument('-t', '--type', type=str, default='st', help='one of: st (standart), w1(Winograd), w2(Winograd, optimized)')
    x.add_argument('-a', '--analyse', action='store_const', const=True, help='measure time')
    x.set_defaults(func=lambda data: prod(data))

    x = subparsers.add_parser(name='graph', prog='graph', help="compare algorithms work time")
    x.add_argument('-l', '--left', type=int, default=10, help='left string-length border')
    x.add_argument('-r', '--right', type=int, default=100, help='right string-length border')
    x.add_argument('-s', '--step', type=int, default=10, help='step')
    x.add_argument('-ru', '--russian', action='store_const', const=True, help='use Russian language for graph labels')
    x.add_argument('-t', '--time', type=float, default=0.1, help='time in secs for each measurement')
    x.add_argument('-c', '--case', type=str, default='best', help='one of: best(default), worst')
    x.set_defaults(func=lambda data: graph(data))

    return parser


if __name__ == '__main__':
    parser = create_console_parser()

    hello = '''This software is used to calculate Levenstein and Damerau-Levenstein distances between two strings using
different algorithms. Also it can calculate execution time. To input an empty string, type '' (two single quotes).'''
    CONSOLE = User_console(parser, hello=hello)
    print('>> ', end='')
    while True:
        CONSOLE.read_line()
        print('>> ', end='')
