import numpy as np
import itertools

def standart(a: np.array, b: np.array):
	if len(a[0]) != len(b): return []
	n, m, q = len(a), len(b[0]), len(a[0])
	c = [0] * n
	for i in range(n):
		c[i] = [0] * m

	for i in range(n):
		for j in range(m):
			for k in range(q):
				c[i][j] += a[i][k] * b[k][j]

	return c


def winograd(a: np.array, b: np.array):
	if len(a[0]) != len(b): return []
	n, m, q = len(a), len(b[0]), len(a[0])
	c = [0] * n
	for i in range(n):
		c[i] = [0] * m

	ax = [0]*n
	for i in range(n):
		for k in range(q//2):
			ax[i] += a[i][2*k] * a[i][2*k+1]

	bx = [0]*m
	for j in range(m):
		for k in range(q//2):
			bx[j] += b[2*k][j] * b[2*k+1][j]

	for i in range(n):
		for j in range(m):
			c[i][j] = -ax[i] - bx[j]
			if q % 2: c[i][j] += a[i][q-1] * b[q-1][j]
			for k in range(q//2):
				c[i][j] += (a[i][2*k] + b[2*k+1][j])*(a[i][2*k+1] + b[2*k][j])

	return c


def winograd_optimized(a: np.array, b: np.array):
	if len(a[0]) != len(b): return []
	n, m, q = len(a), len(b[0]), len(a[0])
	c = [0] * n
	for i in range(n):
		c[i] = [0] * m

	bx = [0]*m
	for j in range(m):
		for k in range(0, q-1, 2):
			bx[j] -= b[k][j] * b[k+1][j]

	for i in range(n):
		ax = 0
		for k in range(0,q-1,2):
			ax -= a[i][k] * a[i][k + 1]
		for j in range(m):
			x = ax + bx[j]
			if q % 2: x += a[i][q-1] * b[q-1][j]
			for k in range(0,q-1,2):
				x += (a[i][k] + b[k+1][j])*(a[i][k+1] + b[k][j])
			c[i][j] = x
	return c


'''def standart(a: np.array, b: np.array):
	if a.shape[1] != b.shape[0]: return []
	n, m, q = a.shape[0], b.shape[1], a.shape[1]
	c = np.full([n, m], 0)

	for i, j, k in itertools.product(range(n), range(m), range(q)):
		c[i][j] += a[i][k] * b[k][j]

	return c


def winograd(a: np.array, b: np.array):
	if a.shape[1] != b.shape[0]: return []
	n, m, q = a.shape[0], b.shape[1], a.shape[1]
	c = np.full([n, m], 0)

	ax = np.full(n, 0)
	for i, k in itertools.product(range(n), range(q//2)):
		ax[i] += a[i][2*k] * a[i][2*k+1]

	bx = np.full(m, 0)
	for j, k in itertools.product(range(m), range(q//2)):
		bx[j] += b[2*k][j] * b[2*k+1][j]

	for i, j in itertools.product(range(n), range(m)):
		c[i][j] = -ax[i] - bx[j]
		if q % 2: c[i][j] += a[i][q-1] * b[q-1][j]
		for k in range(q//2):
			c[i][j] += (a[i][2*k] + b[2*k+1][j])*(a[i][2*k+1] + b[2*k][j])

	return c


def winograd_optimized(a: np.array, b: np.array):
	if a.shape[1] != b.shape[0]: return []
	n, m, q = a.shape[0], b.shape[1], a.shape[1]
	c = np.full([n, m], 0)

	bx = np.full(m, 0)
	for j, k in itertools.product(range(m), range(0,q-1,2)):
		bx[j] -= b[k][j] * b[k+1][j]

	for i in range(n):
		ax = 0
		for k in range(0,q-1,2):
			ax -= a[i][k] * a[i][k + 1]
		for j in range(m):
			c[i][j] = ax + bx[j]
			if q % 2: c[i][j] += a[i][q-1] * b[q-1][j]
			for k in range(0,q-1,2):
				c[i][j] += (a[i][k] + b[k+1][j])*(a[i][k+1] + b[k][j])

	return c'''