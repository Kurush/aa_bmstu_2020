import unittest
from matrix_prod import *


class SortsTest(unittest.TestCase):
    def test1(self):
        self._test([[2]], [[3]], [[6]])

    def test3(self):
        self._test([[1,2,3]], [[1],[2],[3]], [[14]])

    def test4(self):
        self._test([[1,2,3,4]], [[1],[2],[3],[4]], [[30]])
	
    def test_sql(self):
        self._test([[2,0,0,0],[0,3,0,0],[0,0,4,0],[0,0,0,5]], [[1],[1],[1],[1]], [[2],[3],[4],[5]])

    def test_sqr(self):
        self._test([[1,1,1,1]], [[2,0,0,0],[0,3,0,0],[0,0,4,0],[0,0,0,5]], [[2,3,4,5]])

    def _test(self, a, b, c):
        a, b, c = np.array(a), np.array(b), np.array(c)

        self.assertEqual(np.all(standart(a, b) == c), True)
        self.assertEqual(np.all(winograd(a, b) == c), True)
        self.assertEqual(np.all(winograd_optimized(a, b) == c), True)

if __name__ == '__main__':
    unittest.main()
