from time import process_time
import tracemalloc


def measure_memory(f, *args, **kwargs):
    tracemalloc.start()
    f(*args, **kwargs)
    snapshot = tracemalloc.take_snapshot()
    top_stats = snapshot.statistics('lineno')

    mem = 0
    for stat in top_stats:
        if stat.traceback._frames[0][0].find('string_distances') != -1 or \
           stat.traceback._frames[0][0].find('levenstein_distances') != -1:
            mem += stat.size // stat.count
            break
    return mem


def measure_time(f, *args, _n=10, absolute_time=False, **kwargs):
    if absolute_time:
        time = measure_time(f, *args, _n=1, **kwargs)
        if time > _n: return time
        cnt = int(_n / time)
        time_sum = measure_time(f, *args, _n=cnt, **kwargs)

        return (time_sum * cnt + time) / (cnt + 1)

    start = process_time()
    for _ in range(_n):
        f(*args, **kwargs)
    end = process_time()

    return (end - start) / _n
