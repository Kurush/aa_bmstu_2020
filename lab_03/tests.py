import unittest
from lab_02.sorts import *


class SortsTest(unittest.TestCase):
    def test_empty(self):
        self._test([])

    def test_1(self):
        self._test([1])

    def test_sorted(self):
        self._test([1,2,3,4,5,6,7,8,9,10])

    def test_reversed(self):
        self._test([5,4,3,2,1,0,-1,-2,-3,-4])

    def test_pos_neg(self):
        self._test([1,-1,2,-2,3,-3,4,-4,0,0])

    def test_odd(self):
        self._test([3, 1, 2])

    def test_even(self):
        self._test([4, 1, 2, 3])

    def _test(self, arr):
        arr = np.array(arr)
        arr1, arr2, arr3 = np.copy(arr), np.copy(arr), np.copy(arr)
        sorted_arr = np.sort(arr)
        insert_sort(arr1)
        shaker_sort(arr2)
        gnome_sort(arr3)
        self.assertEqual(np.all(arr1 == sorted_arr), True)
        self.assertEqual(np.all(arr2 == sorted_arr), True)
        self.assertEqual(np.all(arr3 == sorted_arr), True)


if __name__ == '__main__':
    unittest.main()
