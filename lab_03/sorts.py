import numpy as np


def insert_sort(arr: np.array):
    for i in range(1, arr.shape[0]):
        key = arr[i]
        j = i-1
        while key < arr[j] and j >= 0:
            arr[j+1] = arr[j]
            j -= 1
        arr[j+1] = key


def shaker_sort(arr: np.array):
	start, end = 0, arr.shape[0] - 1
	swapped = True
	while swapped:
		swapped = False
		for i in range(start, end):
			if arr[i] > arr[i + 1]:
				arr[i], arr[i + 1] = arr[i + 1], arr[i]
				swapped = True

		if not swapped: break
		end = end - 1

		swapped = False
		for i in range(end - 1, start - 1, -1):
			if arr[i] > arr[i + 1]:
				arr[i], arr[i + 1] = arr[i + 1], arr[i]
				swapped = True
		start = start + 1

def gnome_sort(arr: np.array):
	i, size = 1, arr.shape[0]
	while i < size:
		if arr[i - 1] > arr[i]:
			arr[i - 1], arr[i] = arr[i], arr[i - 1]
			i = i-1 if i > 1 else i
		else: i += 1


def sorted_case(n, start=-1000):
	return np.array(range(start, start+n))

def reverse_sorted_case(n, start=1000):
	return np.array(range(start, start-n, -1))

def random_case(n, l=-1000, r=1000):
	return np.random.uniform(l, r, n)