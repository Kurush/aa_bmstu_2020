import sys
sys.path.append("..")

from console.console import *
from termcolor import colored
import matplotlib.pyplot as plt
import numpy as np
import os

import lab_03.sorts as sorts
from measure_time import *

sort_name = {'s1': 'insertion sort',
        's2': 'gnome sort',
        's3': 'shaker sort',
        }

sort_cases = {
    # todo
    's1': {
        'best': sorts.sorted_case,
        'worst': sorts.reverse_sorted_case,
        'random': sorts.random_case
    },
    's2': {
        'best': sorts.sorted_case,
        'worst': sorts.reverse_sorted_case,
        'random': sorts.random_case
    },
    's3': {
        'best': sorts.sorted_case,
        'worst': sorts.reverse_sorted_case,
        'random': sorts.random_case
    }
}

def choose_function(algo):
    if algo == 's1':
        f = sorts.insert_sort
    elif algo == 's2':
        f = sorts.gnome_sort
    elif algo == 's3':
        f = sorts.shaker_sort
    else:
        f = None
    return f


def sort_values(data):
    try:
        f = choose_function(data.type)
        arr = np.loadtxt(os.getcwd() + '/' + data.file)
        print('sorting:', arr)
        f(arr)
        np.savetxt(os.getcwd() + '/' + data.file+'_sorted', arr, newline=' ', fmt='%.2f')
        info = 'sort type:' + sort_name[data.type]

        print(info)
        print('result:', arr)
        if data.analyse:
            tm = measure_time(f, arr, _n=1000)
            print('execute time:', round(tm * 1000, 4), 'msecs')
        print('====----====')
    except Exception as e:
        print(colored("error: " + str(e), 'red'))
        return


def graph(data):
    times = {'s1': [], 's2': [], 's3': []}
    rg = range(data.left, data.right, data.step)
    for n in rg:
        for algo in times.keys():
            arr = sort_cases[algo][data.case](n)
            f = choose_function(algo)
            times[algo].append(measure_time(f, arr, absolute_time=True, _n=data.time)*1000)

    rg = list(rg)
    with open('measure_results.txt', 'w') as f:
        for algo in times.keys():
            f.write(algo + ' ')
        f.write('\n')
        for i in range(len(rg)):
            f.write(str(rg[i]) + ' ')
            for algo in times.keys():
                f.write(str(round(times[algo][i], 2)) + ' ')
            f.write('\n')

    for algo in times.keys():
        plt.plot(rg, times[algo])
    plt.legend([sort_name[x] for x in times.keys()])
    if data.russian == True:
        plt.ylabel('время выполнения, мс.')
        plt.xlabel('длина массива, n')
        case = 'лучший' if data.case=='best' else 'худший' if data.case=='worst' else 'произвольный'
        plt.title('Сравнительный анализ сортировок: %s случай' % case)
    else:
        plt.ylabel('execute time, msec')
        plt.xlabel('array length, n')
        plt.title('Comparative analysis: %s case' % data.case)
    plt.xticks(range(data.left, data.right, int((data.right - data.left) // 9)))
    plt.show()



def create_console_parser():
    parser = ThrowingArgumentParser(prog='PROG')
    subparsers = parser.add_subparsers()

    x = subparsers.add_parser(name='sort', prog='s1', help="sort numbers in file")
    x.add_argument('-f', '--file', type=str, default='numbers.txt', help='file with numbers to sort')
    x.add_argument('-t', '--type', type=str, default='s1', help='one of: s1 (insertion), s2(gnome), s3(shaker)')
    x.add_argument('-a', '--analyse', action='store_const', const=True, help='measure time')
    x.set_defaults(func=lambda data: sort_values(data))

    x = subparsers.add_parser(name='graph', prog='graph', help="compare algorithms work time")
    x.add_argument('-l', '--left', type=int, default=100, help='left string-length border')
    x.add_argument('-r', '--right', type=int, default=1001, help='right string-length border')
    x.add_argument('-s', '--step', type=int, default=100, help='step')
    x.add_argument('-ru', '--russian', action='store_const', const=True, help='use Russian language for graph labels')
    x.add_argument('-t', '--time', type=float, default=0.1, help='time in secs for each measurement')
    x.add_argument('-c', '--case', type=str, default='random', help='one of: best, worst, random(default)')
    x.set_defaults(func=lambda data: graph(data))

    return parser


if __name__ == '__main__':
    parser = create_console_parser()

    hello = '''This software is used to calculate Levenstein and Damerau-Levenstein distances between two strings using
different algorithms. Also it can calculate execution time. To input an empty string, type '' (two single quotes).'''
    CONSOLE = User_console(parser, hello=hello)
    print('>> ', end='')
    while True:
        CONSOLE.read_line()
        print('>> ', end='')